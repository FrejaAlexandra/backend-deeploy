import { Controller, Get, Req } from '@nestjs/common';
import { Request } from 'express';
import { AppService } from './app.service';
import { map } from 'rxjs/operators';

@Controller()
export class AppController {
  constructor(private readonly appService: AppService) {}

  @Get()
  getHello(): string {
    return this.appService.getHello();
  }

  @Get('logs')
  getLogs(@Req() request: Request): any[] {
    console.log(request.query);
    if (request.query.model) {
      return this.appService.getFilterLogs(request.query.model as string);
    } else {
      return this.appService.getLogs();
    }
  }

  @Get('models')
  getModels(@Req() request: Request): any[] {
    console.log(request.query);
    //get distinct values unfinished
    if (request.query.model) {
      return this.appService.getFilterModels(request.query.model as string);
    } else {
      return this.appService.getModels();
    }
  }
}
