import { Injectable } from '@nestjs/common';
import { loadavg } from 'os';
import * as data from '../data/data.json';

@Injectable()
export class AppService {
  getHello(): string {
    return 'Hello World!';
  }

  getLogs(): any[] {
    return data;
  }

  getFilterLogs(model: string): any[] {
    return data.filter((log: any) => log.model == model);
  }

  getModels(): string[] {
    return data.map((log: any) => log.model);
  }

  getFilterModels() {
    return data.filter((log: any) => log.model);
  }
}
